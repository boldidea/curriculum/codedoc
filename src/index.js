import MarkdownIt from 'markdown-it';
import MarkdownItContainer from 'markdown-it-container';
import MarkdownItAttrs from 'markdown-it-attrs';
import MarkdownItKbd from 'markdown-it-kbd';
import MarkdownItBracketedSPans from 'markdown-it-bracketed-spans';
import MarkdownItTaskLists from 'markdown-it-task-lists';

import hljs from './highlight.pack';
import arrowCreate from './arrows';

let __domContentHasLoaded = false;

function urljoin(...parts) {
  // strip trailing slash from each part but last
  parts = parts.map((path, i) => i+1 == parts.length ? path : path.replace(/\/$/, ''));
  return parts.join('/');
}

export default class CodeDoc extends EventTarget {

  constructor(opts) {
    super();

    opts = opts || {};
    this.opts = opts;
    if (this.opts.index) {
      const pathParts = this.opts.index.split('/');
      this.opts.chaptersPathPrefix = pathParts.length > 1 ? pathParts[0] : '';
    }
    this.opts.containerNames = opts.containerNames || [];
    this.opts.cacheTag = opts.cacheTag || (new Date()).getTime();

    // Setup markdown support
    this.md = new MarkdownIt({
      html: true,
      typographer: true
    });
    this.md.use(MarkdownItAttrs);
    this.md.use(MarkdownItKbd);
    this.md.use(MarkdownItBracketedSPans);
    this.md.use(MarkdownItTaskLists, {label: true, enabled: true});

    const containerElements = ['div', 'address', 'article', 'aside', 'footer', 'header', 'hgroup', 'main', 'nav', 'section'];
    const containerNames = [...containerElements, 'infobox', 'narrated-code', 'narration', 'row']
    containerNames.push(...this.opts.containerNames);

    for (let containerName of containerNames) {
      const re = new RegExp(`^${containerName}(\\s+(.+))?$`);
      this.md.use(MarkdownItContainer, containerName, {
        validate: (params) => {
          const isValid = params.trim().match(re);
          //console.log(`Validate: "${params.trim()}"`, isValid);
          return isValid;
        },
        render: (tokens, idx) => {
          //console.log(tokens[idx].info.trim());

          let tagName;
          const classNames = [];
          const attrs = {};

          // if the containerName is not a HTML element, use a div
          if (containerElements.indexOf(containerName) === -1) {
            tagName = 'div';
            classNames.push(containerName);
          } else {
            tagName = containerName;
          }

          if (tokens[idx].nesting === 1) {
            var m = tokens[idx].info.trim().match(re);

            // parse class names, id's, attrs (similar to markdowin-it-attrs)
            if (m[2]) {
              // allow markdown-it-attrs style {...}
              const args = m[2].trim().replace(/^{|}$/g, '');

              for (let token of args.split(/\s+/)) {
                const idMatch = token.match(/^#(.+)/);
                const classMatch = token.match(/^\.(.+)/);
                const attrMatch = token.match(/^([^=]+)="(.*)"/);
                if (idMatch) {
                  attrs.id = idMatch[1];
                } else if (classMatch) {
                  classNames.push(classMatch[1]);
                } else if (attrMatch) {
                  attrs[attrMatch[1]] = attrMatch[2];
                } else {
                  // assume any other parameters are just class names
                  classNames.push(token);
                }
              }
            }
            if (classNames.length) attrs.class = classNames.join(' ');
            const attrStr = Object.entries(attrs).map(([k, v]) => `${k}="${v}"`).join(' ');
            return `<${tagName}${attrStr ? ' ' + attrStr : ''}>\n`;
          } else {
            return `</${tagName}>\n`;
          }
        }
      });
    }

    // initialize properties
    this.root = null;
    this.chapters = null;
    this.chapterIndex = {};
    this.initialChapter = null;
    this.currentChapter = null;

    document.addEventListener('DOMContentLoaded', this.domReady);
  }

  scrollToId(id) {
    let el = this.root.getElementById(id);
    if (el) {
      const pagedjsEl = el.closest('.pagedjs_page');
      if (pagedjsEl) el = pagedjsEl;
      el.scrollIntoView();
    }
  }

  createChapterElement(container, chapter) {
    // create container for chapter content
    const chapterEl = document.createElement('section');
    chapterEl.classList.add('chapter');
    if (chapter) {
      chapterEl.setAttribute('id', chapter.path);
      chapterEl.classList.add(...chapter.path.split('.'));
    }
    container.appendChild(chapterEl);

    return chapterEl;
  }

  loadChapters(container) {
    // load each chapter into a list of promises
    const chapterPromises = [];
    for (let path in this.chapterIndex) {
      const chapter = this.chapterIndex[path];
      if (!chapter.src) continue;

      if (this.initialChapter === null) this.initialChapter = chapter.path;

      const chapterEl = this.createChapterElement(container, chapter);
      chapter.element = chapterEl;

      // load the chapter
      const url = urljoin(this.opts.chaptersPathPrefix, chapter.src) + '?' + this.opts.cacheTag;
      console.debug("Loading chapter", chapter.path, url);
      const promise = fetch(url)
        .then(response => response.text())
        .then(content => url.match(/\.md(\?.*)?$/) ? this.md.render(content) : content)
        .then(content => chapterEl.innerHTML = content)
        .then(() => {
          this.dispatchEvent(new CustomEvent('chapter-loaded', {
            detail: {
              path: chapter.path,
              el: chapter.el
            }
          }));
        });
      chapterPromises.push(promise);
    }
    return Promise.all(chapterPromises);
  }

  init(container, markdown) {
    this.root = container;

    return Promise.resolve().then(() => {
      if (this.opts.index) {
        // Load pages from chapterIndex
        const indexUrl = this.opts.index + '?' + this.opts.cacheTag;
        return fetch(indexUrl).then(response => response.json()).then(index => {
          console.debug(`got ${indexUrl}`, index);
          this.chapters = this.processIndex(index);
          return this.loadChapters(this.root, this.chapters).then(() => {
            this.dispatchEvent(new CustomEvent('all-chapters-loaded'));
            // connect hashChange event
            window.addEventListener('hashchange', () => this.hashChange());
          });
        });
      } else if (markdown) {
        // Load a single page from the given markdown
        const chapterEl = this.createChapterElement(container);
        const content = this.md.render(markdown);
        chapterEl.innerHTML = content;
        chapterEl.classList.add('active');
        this.dispatchEvent(new CustomEvent('chapter-loaded', {
          detail: {
            path: null,
            el: chapterEl
          }
        }));
        return Promise.resolve();
      } else {
        throw new Error(
          'You must either set `opts.index` in the constructor, or use `init(container, markdown)`.'
        );
      }
    }).then(() => {
      // do all the post-processing
      this.initCodeBlocks();
      this.initPopups();
      this.autoLinkHeadings();
      this.redraw();

      if (this.opts.index) {
        // build toc if ther'es a .toc-container element
        this.root.querySelectorAll('.toc-container').forEach(parent => this.buildTOC(parent));
      }

      // fire loaded event
      this.dispatchEvent(new CustomEvent('loaded'));

      if (this.opts.index) {
        // call initial hashchange
        this.hashChange();
      }

      return Promise.resolve()
    });
  }

  getChapter(path) {
    return this.chapterIndex[path];
  }

  processIndex(index, parent) {
    const chapters = [];
    let itemIndex = 0;
    for (let id in index) {
      let chapterObj;
      if (typeof index[id] === 'string') {
        chapterObj = {
          src: index[id]
        }
      } else {
        chapterObj = index[id];
      }

      chapterObj.id = id;
      chapterObj.path = parent ? parent.path + '.' + id : id;
      chapterObj.index = itemIndex;
      chapterObj.parent = parent;
      chapterObj.title = chapterObj.title || null;
      chapters.push(chapterObj);

      // index the chapter for easy access
      this.chapterIndex[chapterObj.path] = chapterObj;

      if (chapterObj.chapters) {
        chapterObj.chapters = this.processIndex(chapterObj.chapters, chapterObj);
      }

      itemIndex++;
    }
    return chapters;
  }

  /**
   * Runs any modifications that need to be done when changing the page
   */
  redraw() {
    this.alignNarratedCode();
    this.addArrows();
  }

  /**
   * Process the rendered markdown (customize code blocks, set up heading links, etc)
   */
  render() {
  }

  initCodeBlocks() {
    // Add class="block" to any <code> inside a <pre>, and move it outside the parent
    // (see here: https://github.com/arve0/markdown-it-attrs/issues/116)
    this.root.querySelectorAll('pre > code').forEach(block => {
      block.classList.add('block');
      const parent = block.parentNode;
      parent.after(block);
      parent.parentNode.removeChild(parent);
    });

    this.root.querySelectorAll('code[class]').forEach(block => this.initCodeBlock(block));
  }

  initCodeBlock(block) {
    this.dedentCode(block);
    this.modifyCopy(block);
    // remove last newline
    block.innerHTML = block.innerHTML.replace(/\n$/, '');
    if (!block.classList.contains('nohl')) hljs.highlightBlock(block);
    if (block.classList.contains('block')) {
      this.markUpTextNodes(block);
      this.addCodeLines(block);
      if (block.classList.contains('python')) {
        this.highlightTracebacks(block);
      }
      if (block.classList.contains('shell')) {
        this.highlightShellOutput(block);
      }
      if (block.classList.contains('console')) {
        this.highlightConsoleLines(block);
      }
    }
  }

  buildTOC(parent) {
    // populates TOC w/ newly added chapter
    if (!parent) {
      console.error("cannot find TOC element");
      return;
    }

    function makeTOCItem(heading) {
      // get chapter element for this heading
      const chapterEl = heading.closest('.chapter');
      const tocItem = document.createElement('li');
      const title = document.createElement(chapterEl.id ? 'a' : 'span');
      title.classList.add('chapter-title');
      if (chapterEl.id) title.setAttribute('href', '#' + chapterEl.id);
      title.textContent = heading.textContent;
      tocItem.appendChild(title);
      return tocItem;
    }

    // Get all headings and subheadings in chapter
    for (let path in this.chapterIndex) {
      // create chapter link
      const heading = this.root.querySelector(`#${path} h1:not(.notoc)`);
      console.log(`#${path} h1:not(.notoc)`);
      if (!heading) continue;
      const tocItem = makeTOCItem(heading);
      parent.appendChild(tocItem);

      // get sub-headings
      const subheadings = this.root.querySelectorAll(`#${path} h2:not(.notoc)`);
      if (subheadings.length > 0) {
        const subList = document.createElement('ul');
        tocItem.appendChild(subList);
        subheadings.forEach(subheading => {
          const subItem = makeTOCItem(subheading);
          subList.appendChild(subItem);
        });
      }
    }
  }

  alignNarratedCode() {
    // aligns code narration with code lines using data-line attr
    // - assumes top of code container is aligned with top of narration container
    // - requires narration container have position: relative, width: 100%;
    this.root.querySelectorAll('.narrated-code.aligned').forEach(container => {
      // get the line-height of the code
      const codeBlock = container.querySelector('code');
      const codeLines = container.querySelectorAll('code .hljs-line');
      let lineStart = 1;
      if (codeBlock.dataset.lineStart) {
        lineStart = parseInt(codeBlock.dataset.lineStart);
      }
      container.querySelectorAll('.narration [data-line]').forEach(line => {
        let lineIndex = parseInt(line.dataset.line) - lineStart;
        line.style.position = 'absolute';
        if (codeLines[lineIndex]) line.style.top = codeLines[lineIndex].offsetTop + 'px';
      });
    });
  }

  initPopups() {
    this.root.querySelectorAll('.modal').forEach(el => {
      const closeRow = document.createElement('div');
      closeRow.classList.add('modal-close');
      const closeBtn = document.createElement('button');
      closeBtn.textContent = 'X';
      closeBtn.setAttribute('title', 'Close');
      closeBtn.addEventListener('click', () => {
        closeBtn.closest('.modal-container').classList.remove('active');
      });
      closeRow.appendChild(closeBtn);
      el.prepend(closeRow);

      // move modal inside container
      const container = document.createElement('div');
      container.classList.add('modal-container');
      el.after(container);
      container.appendChild(el);
      container.setAttribute('id', el.getAttribute('id'));
      el.removeAttribute('id');

      // close container when clicked
      container.addEventListener('click', event => {
        if (event.target === container) {
          container.classList.remove('active');
        }
      });
    });

    // close container if escape key is pressed
    document.addEventListener('keyup', event => {
      if (event.key == 'Escape') {
        document.querySelectorAll('.modal-container').forEach(el => {
          el.classList.remove('active');
        });
      }
    });

    this.root.querySelectorAll('.modal-trigger').forEach(el => {
      el.addEventListener('click', event => {
        event.preventDefault();
        const sel = el.getAttribute('href');
        const modalEl = document.querySelector(sel);
        if (!modalEl) {
          throw new Error('Invalid selector: ', sel);
        }
        modalEl.classList.add('active');
      });
    });
  }

  autoLinkHeadings(onPageChanged) {
    // Auto-links headings to the closest id
    this.root.querySelectorAll('h1').forEach(heading => {
      heading.style.cursor = 'pointer';
      heading.addEventListener('click', () => {
        const id = heading.closest('[id]').id;
        if (!onPageChanged) {
          window.location.href = '#' + id;
        } else {
          history.pushState({}, '', '#' + id);
          onPageChanged(id);
        }
      });
    });
  }

  modifyCopy(el) {
    el.addEventListener('copy', event => {
      let data = document.getSelection().toString();
      // strip out zero-width space chars 
      data = data.replace(/\u200B/g, '');
      event.clipboardData.setData('text', data);
      event.preventDefault();
    });
  }

  dedentCode(el) {
    if (el.textContent.indexOf('\n') === -1) return;
    let content = el.textContent.replace(/(^\s*\n|\n\s*$)/g, '');

    // find the smallest indent
    var match = content.match(/^(\s+|\S)/, 'gm')
    let indent = null;
    const indents = [];
    for (const line of content.split('\n')) {
      const indentMatch = line.match(/^\s+/);
      const lineIndent = indentMatch ? indentMatch[0] : '';
      if (indent === null || lineIndent.length < indent.length) {
        indent = lineIndent;
      }
    }
    const re = new RegExp('^' + indent, 'gm');
    const newIndent = el.dataset.indent ? parseInt(el.dataset.indent) : 0;
    el.textContent = content.replace(re, ' '.repeat(newIndent));
  }

  markUpTextNodes(block) {
    // hljs does not wrap some pieces of syntax. This makes it difficult to target elements using
    // javascript if we want to. To remedy this, we wrap all text nodes in their own <span>.
    function walk(node) {
      let hasElementNodes = false;
      let hasTextNodes = false;
      for (const child of node.childNodes) {
        if (node.nodeType === Node.TEXT_NODE) hasTextNodes = true;
        if (node.nodeType === Node.ELEMENT_NODE) hasElementNodes = true;
        if (hasElementNodes && hasTextNodes) break;
      }
      if (hasElementNodes && hasTextNodes) {
        for (const [i, child] of node.childNodes.entries()) {
          
        }
      }
    }
  }

  addCodeLines(block) {
    if (!block.classList.contains('continue')) {
      // KLUDGE: hljs likes to override counter-reset and counter-increment for some reason
      block.style.counterReset = 'hljs-line-number';
      block.style.counterIncrement = 'hljs-line-number 0';
    }
    let lineStart = 1;
    if (block.dataset.lineStart) {
      lineStart = parseInt(block.dataset.lineStart);
      // KLUDGE: (see above)
      block.style.counterReset = 'hljs-line-number ' + (lineStart -1);
      block.style.counterIncrement = 'hljs-line-number 0';
    }

    const lineMatch = block.textContent.match(/\n/g);
    const numLines = (lineMatch ? lineMatch.length : 0) + lineStart;

    function addLineSpans(content, extraClass) {
      // Before splitting into lines, look for template literals that span multiple lines,
      // and break up the .hljs-string spans into one per line
      const multiLineRe = /<span class="hljs-(string|comment)">((\/\*|`).*?(\*\/|`))<\/span>/gms;
      let match = null;
      let replacements = [];
      while ((match = multiLineRe.exec(content)) !== null) {
        const type = match[1];
        const string = match[2];
        const tokenStart = match[3];
        const tokenEnd = match[4];
        if (string.indexOf('\n') === -1) continue;
        // we add the class multiline to prevent the span from being re-processed later
        const fixedContent = `<span class="hljs-${type} multiline">`
          + string.replace(/\n/gm, `</span>\n<span class="hljs-${type} multiline">`)
          + `</span>`;
        replacements.push({match, fixedContent});
      }

      let offset = 0;
      for (const {match, fixedContent} of replacements) {
        content = content.slice(0, offset + match.index, match[0].length)
          + fixedContent
          + content.slice(offset + match.index + match[0].length);
        // as we modify the string, index positions will change
        offset = fixedContent.length - match[0].length;
      }

      // Now we split the line up and wrap each in a .hljs-line span
      const lines = content.split('\n');
      extraClass = extraClass ? ' ' + extraClass : '';
      for (let [i, line] of lines.entries()) {
        if (!line.match(/<span class="hljs-line/)) {
          if (line === '') line = '&#8203';  // prevent line from collapsing
          lines[i] = `<span class="hljs-line${extraClass}">${line}</span>`;
        }
      }
      return lines.join('\n');
    }

    // KLUDGE: In the next step, we want to wrap each line in a span.hljs-line. But when you have
    // mixed html/js, hljs adds <span class="javascript"> around the whole code block (spanning
    // multiple lines). This messes up our ability to add line spans

    // The solution is to find each instance of <span class="javascript">, and wrap each line inside
    // with <span class="hljs-line javascript">

    // This part is horrible. May God have mercy on my soul.
    const jsWrapperRegExp = /<span class="(javascript|css)">/gs;
    const leadingNewlineRegExp = /^\s*\n/g;
    const trailingNewlineRegExp = /\n\s*$/g;
    // Note: we're using "js" prefix on variables below, but this could be js or css content
    const jsWrapperMatch = jsWrapperRegExp.exec(block.innerHTML);
    if (jsWrapperMatch) {
      const splices = [];
      jsWrapperRegExp.lastIndex = 0; // reset the regexp
      let match = null;
      const contentTypeIndexes = {
        'css': 0,
        'javascript': 0,
      };
      while ((match = jsWrapperRegExp.exec(block.innerHTML)) !== null) {
        const contentType = match[1];
        const i = contentTypeIndexes[contentType];
        const jsContentEl = block.querySelectorAll(`span[class="${contentType}"]`)[i];
        let jsContent = jsContentEl.innerHTML;
        let origJsContent = jsContent;
        const origLength = (`<span class="${contentType}">` + jsContent + '</span>').length;

        // if jsContent starts/ends with newline, strip those off, but keep them for later
        let leadingNewline = '', trailingNewline = '';
        let leadingNewlineMatch = null, trailingNewlineMatch = null;
        if ((leadingNewlineMatch = leadingNewlineRegExp.exec(jsContent)) !== null) {
          jsContent = jsContent.slice(leadingNewlineMatch[0].length);
          leadingNewline = leadingNewlineMatch[0];
        }
        if ((trailingNewlineMatch = trailingNewlineRegExp.exec(jsContent)) !== null) {
          jsContent = jsContent.slice(0, trailingNewlineMatch.index);
          trailingNewline = trailingNewlineMatch[0];
        }
        if (leadingNewline || trailingNewline) {
          jsContent = leadingNewline + addLineSpans(jsContent, contentType) + trailingNewline;
        } else {
          jsContent = `<span class="${contentType}">` + jsContent + '</span>';
        }

        const index = match.index;
        splices.push({jsContent, origJsContent, jsContentEl, index});

        contentTypeIndexes[contentType] += 1;
      }

      for (let {jsContentEl} of splices) {
        jsContentEl.parentNode.removeChild(jsContentEl);
      }

      let content = block.innerHTML;
      let offset = 0;
      for (let splice of splices) {
        const {jsContent, index, jsContentEl, origJsContent} = splice;
        content = content.slice(0, offset + index) + jsContent + content.slice(offset + index);
        offset += jsContent.length - origJsContent.length - '<span class="javascript"></span>'.length;
      }
      block.innerHTML = content;
    }
    block.innerHTML = addLineSpans(block.innerHTML);

    block.classList.add('hljs-linenumber-size-' + numLines.toString().length);

    // set custom line styles
    let lineStyles = {
      'added-lines': 'added',
      'removed-lines': 'removed',
      'em-lines': 'em',
      'sep-after-lines': 'sep-after',
      'error-lines': 'error'
    }
    for (let className in lineStyles) {
      this.styleCodeLines(block, className, lineStyles[className], lineStart);
    }

    // adjust line number to account for removed lines
    let currentLine = lineStart;
    let removedStart = null;
    let counterReset = null;
    const lines = block.querySelectorAll('.hljs-line');
    lines.forEach((line, i) => {
      if (counterReset) {
        line.style.counterReset = 'hljs-line-number ' + counterReset;
        counterReset = null;
      }
      if (line.classList.contains('removed-start')) {
        removedStart = currentLine; 
      }
      if (line.classList.contains('removed-end')) {
        counterReset = removedStart - 1;
        currentLine = removedStart;
        removedStart = null;
        line.classList.add('line-' + currentLine);
      } else {
        line.classList.add('line-' + currentLine);
        currentLine += 1;
      }
      if (i === 0) line.classList.add('first');
      if (i === lines.length - 1) line.classList.add('last');
    });
  }

  styleCodeLines(block, className, lineClass, lineStart) {
    lineStart = lineStart ? lineStart : 1;
    let key = camelCase(className);
    if (block.dataset[key]) {
      let lineNumbers = parseRange(block.dataset[key]);
      let lines = block.querySelectorAll('.hljs-line');
      for (let i=0; i<lineNumbers.length; i++) {
        let lineNum = lineNumbers[i];
        let prevLineNum = lineNumbers[i-1];
        let nextLineNum = lineNumbers[i+1];
        let lineIndex = lineNum - lineStart;
        let line = lines[lineIndex];
        if (line) {
          line.classList.add(lineClass)
          if (!prevLineNum || prevLineNum != lineNum-1) {
            line.classList.add(lineClass + '-start');
          }
          if (!nextLineNum || nextLineNum != lineNum+1) {
            line.classList.add(lineClass + '-end');
          }
        } else {
          console.debug('Line ' + lineNum + ' not found in code block:', block);
        }
      }
    }
  }

  addArrows() {
    // remove all arrow-curve elements
    this.root.querySelectorAll('svg.arrow-curve').forEach(arrow => {
      arrow.parentNode.removeChild(arrow);
    });
    //console.clear();
    this.root.querySelectorAll('.arrow-def').forEach(def => {
      console.group(def);
      const opts = def.dataset;
      const sheet = def.closest('.pagedjs_page_content');
      const parent = sheet ? sheet : def.parentNode;
      parent.style.position = 'relative';
      console.log('parent:', parent);

      opts.fromTranslate = opts.fromTranslate || '0,0';
      opts.toTranslate = opts.toTranslate || '0,0';

      const arrowConfig = {
        className: 'arrow-curve',
        from: {
          direction: opts.fromDirection,
          node: parent.querySelector(opts.from),
          translation: opts.fromTranslate.split(',').map(s => parseFloat(s))
        },
        to: {
          direction: opts.toDirection,
          node: parent.querySelector(opts.to),
          translation: opts.toTranslate.split(',').map(s => parseFloat(s))
        },
        parent: parent,
      };
      console.log('arrowConfig', arrowConfig);

      if (!arrowConfig.from.node) {
        console.error('could not find ', opts.from);
        return;
      }
      if (!arrowConfig.to.node) {
        console.error('could not find ', opts.to);
        return;
      }

      const arrow = arrowCreate(arrowConfig);
      parent.appendChild(arrow.node);

      let offset;
      if (opts.offset) {
        offset = opts.offset.split(',').map(s => parseInt(s)); 
      } else {
        offset = [0, 0]; 
      }
      console.log(arrow.node);
      console.log(opts);
      const left = parseFloat(arrow.node.style.left);
      const top = parseFloat(arrow.node.style.top);
      console.log('top:', top);
      console.groupEnd();
      arrow.node.style.left = (left + offset[0]) + 'px';
      arrow.node.style.top = (top + offset[1]) + 'px';
    });
  }

  highlightTracebacks(block) {
    // make tracebacks red (must be done after adding code lines)
    let traceback = false;
    block.querySelectorAll('.hljs-line').forEach(line => {
      if (line.textContent.match(/Traceback \(most recent call last\):/g)) {
        traceback = true;
      } else if (line.textContent.match(/^>>> /g)) {
        traceback = false;
      }
      if (traceback) {
        line.classList.add('traceback');
      }
    });
  }

  highlightShellOutput(block) {
    // must be called after addCodeLines 
    block.querySelectorAll('.hljs-line').forEach(line => {
      if (!line.innerText.match(/^[\s\u200B]*>>>/)) {
        line.classList.add('hljs-output');
      }
    });
  }

  highlightConsoleLines(block) {
    block.querySelectorAll('.hljs-line').forEach(line => {
      if (line.innerHTML.match(/^\&gt; /)) {
        line.innerHTML = line.innerHTML.replace(/^&gt; /, '<span class="input-prompt">&gt;</span>');
        line.classList.add('hljs-input');
      } else if (line.innerHTML.match(/^\&lt; /)) {
        if (line.innerHTML.match(/^&lt; <span class="hljs-literal">undefined<\/span>/)) {
          line.querySelector('.hljs-literal').classList.add('hljs-undefined');
        }
        line.innerHTML = line.innerHTML.replace(/^&lt; /, '<span class="output-prompt">&lt;</span>');
        line.classList.add('hljs-output');
      } else if (line.innerHTML.match(/^X /)) {
        console.log("Found error line");
        line.innerHTML = line.innerHTML.replace(/^X /, '<span class="error-prompt">X</span>');
        line.classList.add('hljs-error');
      } else if (line.innerHTML.match(/^  /)) {
        line.classList.add('hljs-log');
        line.innerHTML = line.innerHTML.replace(/^  /, '');
      }
    });
  }

  prev() {
    this.goTo(this.prevChapter.path);
  }

  next() {
    this.goTo(this.nextChapter.path);
  }

  goTo(chapterPath) {
    let chapter = this.getChapter(chapterPath);

    // If this is a "parent", redirect to the first item
    if (chapter && chapter.chapters) {
      window.location.href = `#${chapterPath}.${chapter.chapters[0].id}`;
      return;
    }

    // make sure chapter exists
    if (!chapter) {
      throw new Error(`No such chapter: ${chapter}`);
    }

    const chaptersArr = Object.values(this.chapterIndex).filter(chapter => chapter.src);
    const idx = chaptersArr.indexOf(chapter);

    // get prev, next chapters
    let next = null, prev = null;
    if (idx > 0) prev = chaptersArr[idx - 1];
    if (idx < chaptersArr.length - 1) next = chaptersArr[idx + 1];

    const lastChapter = this.currentChapter;
    this.currentChapter = chapter;
    this.nextChapter = next;
    this.prevChapter = prev;

    chapter.element.classList.add('active');
    document.body.dataset.chapter = chapterPath;
    window.scrollTo(0, 0);
    this.redraw();

    this.dispatchEvent(new CustomEvent('nav', {detail: {chapter, lastChapter}}));
  }

  hashChange() {
    document.querySelectorAll('.chapter.active').forEach(el => {
      el.classList.remove('active');
    });

    const hash = window.location.hash || this.initialChapter;
    const path = hash.replace(/^#/, '');

    try {
      this.goTo(path);
    } catch {
      this.goTo(this.initialChapter);
    }

    /*
    const prevLink = document.querySelector('.prev-link a');
    const nextLink = document.querySelector('.next-link a');
    prevLink.setAttribute('href', prev ? '#' + prev : '');
    nextLink.setAttribute('href', next ? '#' + next : '');
    prevLink.style.display = prev ? '' : 'none';
    nextLink.style.display = next ? '' : 'none';
    */
  }

};

// utility functions
function camelCase(str) {
  return str.replace(/-([a-z])/g, g => g[1].toUpperCase());
}

function parseRange(rangeStr) {
  /**
   * returns an array of numbers in the given comma-separated list of ranges
   */
  const pattern = /(([0-9]+)(-([0-9]+))?)(,|$)/g
  const rangeArr = [];
  let match;
  while ((match = pattern.exec(rangeStr)) !== null) {
    let start = parseInt(match[2]);
    let end = parseInt(match[4]);
    if (isNaN(end)) end = start;
    for (let i=start; i <= end; i++) {
      rangeArr.push(i);
    }
  }
  return rangeArr;
}
